import React from "react";


export default class User extends React.Component {

    state = {
        firstName:'',
        lastName: ''
    }

    handleClick = buttonName => {

        fetch(`http://localhost:3001/users/${this.state.firstName}/${this.state.lastName}` , {
        method: "POST",
        headers: {
            'Content-type': 'application/json'
        }}).then((result) => result.json())        
    };

    
    handleInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
        console.log(this.state)
    }

    render() {
        return (
        <form className="user-form">
            <label>
                firstName
                <input onChange={this.handleInput} value={this.state.firstName} type='text' name='firstName'/>
            </label>
            <br/><br/>
            <label>
                lastName
                <input onChange={this.handleInput} value={this.state.lastName} type='text' name='lastName'/>
            </label>
            <br/><br/>
            <input type='submit' value='create' onSubmit={this.handleClick}></input>
        </form>    
        );
    }
}